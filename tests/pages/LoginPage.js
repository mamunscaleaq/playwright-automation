// models/Search.js
class LoginPage {
    /**
     * @param {import('playwright').Page} page 
     */
    constructor(page) {
      this.page = page;
      this.password_field = page.locator('#password');
      this.email_field = page.locator('#signInName');
      this.forgotPassword_link = page.locator('#forgotPassword');
      this.goBack_link = page.locator('#go-back-section');
    }
    
    async navigate(app_url) {
      await this.page.goto(app_url);
    }
    async typeUserName(user) {
        await this.page.fill("#signInName", user);
    }

    async clcikNext(){
        await this.page.click("#continue");
    }


  }
  module.exports = { LoginPage };