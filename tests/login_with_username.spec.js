const { test, expect } = require('@playwright/test');
const valid_user="testuser1@qa.com";
const invalid_user="testuser1qa.com";
const mp2='https://managementportal.scaleaq-dev.net/';
const { LoginPage } = require('./pages/LoginPage');
//npx playwright test /tests/login_with_username.spec.js

test.describe('Login Tests', () => {
    test('Open Management Portal', async ({ page }) => {
        const loginPage = new LoginPage(page);
        await loginPage.navigate(mp2);
        await expect(loginPage.email_field).toBeFocused();
        await loginPage.typeUserName(valid_user);
        await loginPage.clcikNext();
        await expect(loginPage.password_field).toBeFocused();
        await expect(loginPage.goBack_link).toContainText(valid_user);
    });
  
    test('POM test', async ({ page }) => {
        const loginPage = new LoginPage(page);
        await loginPage.navigate(mp2);
        await loginPage.typeUserName(invalid_user);
        await loginPage.clcikNext();
        await expect(page.locator('.attrEntry > .error')).toContainText("Email address is not valid.");
        await expect(loginPage.email_field).toBeFocused();
    });

    test('Reset password Use valid email address with wrong Password ', async ({ page }) => {
        const loginPage = new LoginPage(page);
        await loginPage.navigate(mp2);
        await loginPage.typeUserName(valid_user);
        await loginPage.clcikNext();
        await loginPage.forgotPassword_link.click();
      });
  
  });